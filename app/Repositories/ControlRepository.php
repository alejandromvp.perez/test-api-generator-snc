<?php

namespace App\Repositories;

use App\Control;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ControlRepository
 * @package App\Repositories
 * @version August 9, 2018, 3:17 pm UTC
 *
 * @method Control findWithoutFail($id, $columns = ['*'])
 * @method Control find($id, $columns = ['*'])
 * @method Control first($columns = ['*'])
*/
class ControlRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'value',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Control::class;
    }
}
