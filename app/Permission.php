<?php 

namespace App;
 
use Zizaco\Entrust\EntrustPermission;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends EntrustPermission
{
	use EntrustUserTrait;
    use SoftDeletes;

	
   protected $fillable = [
        'name',
        'display_name',
        'description'
    ];

    public $table = 'permissions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public static $rules = [
        
    ];

     /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'display_name' => 'integer',
        'description' => 'string'
    ];
 
   //establecemos las relacion de muchos a muchos con el modelo Role, ya que un permiso
   //lo pueden tener varios roles y un rol puede tener varios permisos
   public function roles(){
        return $this->belongsToMany('App\Role');
    }
    /*public function roles()
    {
      return $this->belongsToMany(Config::get('entrust.role'), Config::get('entrust.role_user_table'), 'user_id', 'role_id');
    }*/

     public function scopeName($query, $name)
    {
        if (trim($name) != "") {
          $query->where('name','like','%'.$name.'%')
            ->orWhere('description','like','%'.$name.'%')
            ->orWhere('display_name','like','%'.$name.'%'); 
        }
        //dd("scope:".$name);
    }
}