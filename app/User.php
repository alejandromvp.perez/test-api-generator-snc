<?php

namespace App;

use Auth;
use App\Profile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *      definition="User",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="active",
 *          description="active",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use EntrustUserTrait; //hacemos uso del trait en la clase User para hacer uso de sus métodos

    use CanResetPassword;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'remember_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'active' => 'integer'
    ];

    public static $rules = [
        
    ];



    //establecemos las relaciones con el modelo Role, ya que un usuario puede tener varios roles
    //y un rol lo pueden tener varios usuarios
    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function profile(){
        return $this->hasOne('App\Profile', 'user_id');
    }

    public function setPasswordAttribute($valor)
    {
        if (!empty($valor)) {
            # cifrar contaseña
            $this->attributes['password']= bcrypt($valor);
        }
    }

    public static function boot()
    {
        User::created (function ($user) {
            if (!Auth::guest()) {
                 \Log::info('Usuario creado', [
                    'id:'=>Auth::user()->id, 
                    'nombre'=> Auth::user()->name, 
                    'datos' => $user
                ]);
            }else{
                 \Log::info('Usuario creado', [
                    'datos' => $user
                ]);
            }
        });

        User::updated(function ($user) {
            if (!Auth::guest()) {
                 \Log::info('Usuario actualizado', [
                    'id:'=>Auth::user()->id, 
                    'nombre'=> Auth::user()->name, 
                    'datos_new' => $user
                ]);
            }
        });

        User::deleting(function ($user) {
            if (!Auth::guest()) {
                 \Log::info('Usuario eliminado', [
                    'id:'=>Auth::user()->id, 
                    'nombre'=> Auth::user()->name, 
                    'datos_new' => $user
                ]);
            }
        });
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != "") {
          $query->where('name','like','%'.$name.'%')
            ->orWhere('email','like','%'.$name.'%'); 
        }
        //dd("scope:".$name);
    }


}