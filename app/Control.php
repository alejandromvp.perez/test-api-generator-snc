<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Control extends Model
{
    use SoftDeletes;

    public $table = 'controls';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'value',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'value' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


     public function scopeName($query, $name)
    {
        if (trim($name) != "") {
          $query->where('name','like','%'.$name.'%')
            ->orWhere('value','like','%'.$name.'%')
            ->orWhere('description','like','%'.$name.'%'); 
        }
        //dd("scope:".$name);
    }
}
