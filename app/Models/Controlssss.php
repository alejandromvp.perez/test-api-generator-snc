<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Control
 * @package App\Models
 * @version August 9, 2018, 3:17 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property string name
 * @property integer value
 * @property string description
 */
class Control extends Model
{
    use SoftDeletes;

    public $table = 'controls';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'value',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'value' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

     public function scopeName($query, $name)
    {
        if (trim($name) != "") {
          $query->where('name','like','%'.$name.'%')
            ->orWhere('value','like','%'.$name.'%')
            ->orWhere('description','like','%'.$name.'%'); 
        }
        //dd("scope:".$name);
    }
}

