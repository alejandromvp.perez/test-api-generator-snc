<?php 

namespace App;
 
use Zizaco\Entrust\EntrustRole;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Model;

class Role extends EntrustRole
{
    use SoftDeletes;
	use EntrustUserTrait;
	
    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];

    public $table = 'roles';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'display_name' => 'string',
        'description' => 'string'
    ];


    public static $rules = [
        
    ];

    
   //establecemos las relacion de muchos a muchos con el modelo User, ya que un rol 
   //lo pueden tener varios usuarios y un usuario puede tener varios roles
    public function users(){
        return $this->belongsToMany('App\User');
    }
    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != "") {
          $query->where('name','like','%'.$name.'%')
            ->orWhere('description','like','%'.$name.'%')
            ->orWhere('display_name','like','%'.$name.'%'); 
        }
        //dd("scope:".$name);
    }

    
}