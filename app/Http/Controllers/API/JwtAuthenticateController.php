<?php

namespace App\Http\Controllers\API;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;

class JwtAuthenticateController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/me",
     *      summary="muestra el usuario autenticado.",
     *      tags={"Autenticar"},
     *      description="Usuario autenticado",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          description="Bearer {token}",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(
     *             type="string"
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="operacion exitosa.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="exito.",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index()
    {
        return response()->json(['auth'=>Auth::user()]);
    }

    /**
     *
     * @SWG\Post(
     *      path="/authenticate",
     *      summary="Atenticar un usuario.",
     *      tags={"Autenticar"},
     *      description="Atenticar usuario.",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Email ",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(
     *             type="string"
     *          )
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="password",
     *          required=true,
     *          type="string",
     *          @SWG\Schema(
     *              type="string"
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="operacion exitosa.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="token",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function createRole(Request $request){
        // Todo       
    }

    public function createPermission(Request $request){
        // Todo       
    }

    public function assignRole(Request $request){
         // Todo
    }

    public function attachPermission(Request $request){
        // Todo       
    }

}