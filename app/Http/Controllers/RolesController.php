<?php

namespace App\Http\Controllers;

use Flash;
use App\Role;
use Response;
use App\Permission;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\DataTables\RolesDataTable;
use App\Repositories\RolesRepository;
use App\Http\Requests\CreateRolesRequest;
use App\Http\Requests\UpdateRolesRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;

class RolesController extends AppBaseController
{
    /** @var  RolesRepository */
    private $rolesRepository;

    public function __construct(RolesRepository $rolesRepo)
    {
        $this->rolesRepository = $rolesRepo;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'display_name' => 'required',
            'description' => 'required',
        ]);
    }

    /**
     * Display a listing of the Roles.
     *
     * @param RolesDataTable $rolesDataTable
     * @return Response
     */
    public function index(RolesDataTable $rolesDataTable)
    {
        return $rolesDataTable->render('roles.index');
    }

    /**
     * Show the form for creating a new Roles.
     *
     * @return Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created Roles in storage.
     *
     * @param CreateRolesRequest $request
     *
     * @return Response
     */
    public function store(CreateRolesRequest $request)
    {
        $this->validator($request->all())->validate();
        
        $input = $request->all();

        $roles = $this->rolesRepository->create($input);

        Flash::success('Rol creado.');

        return redirect(route('roles.index'));
    }

    /**
     * Display the specified Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Rol no encontrado');

            return redirect(route('roles.index'));
        }

        return view('roles.show')->with('roles', $roles);
    }

    /**
     * Show the form for editing the specified Roles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Rol no encontrado');

            return redirect(route('roles.index'));
        }

        return view('roles.edit')->with('roles', $roles);
    }

    public function editPermissions($id)
    {
        //retorla la vista de de todos permisons de un rol
        $role = Role::find($id);
        $permissions= Permission::orderBy('id','asc')->paginate(0);

        if ( $user = Role::find($id)->users()->orderBy('name')->first() ) {
            # cambiar los permisos
            return view('roles.editPermissions',['permissions'=>$permissions, 'user'=>$user, 'role'=>$role ]);
        }

        flash('Rol sin usuario asignados', 'danger');
        return redirect()->route('roles.index');
    }

    public function postAssignPermissions(Request $request, $id)
    {
        $role = Role::find($id);
        $permissions= Permission::all();

        $role->permissions()->detach();
        foreach ($permissions as $permission) {
            if ($request[$permission->name]) {
                $role->attachPermission($permission);
            }
        }

        flash('Permisos modificados', 'success');
        return redirect()->route('roles.index'); 
    }

    /**
     * Update the specified Roles in storage.
     *
     * @param  int              $id
     * @param UpdateRolesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRolesRequest $request)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Rol no encontrado');

            return redirect(route('roles.index'));
        }

        $roles = $this->rolesRepository->update($request->all(), $id);

        Flash::success('Rol actualizado.');

        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified Roles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roles = $this->rolesRepository->findWithoutFail($id);

        if (empty($roles)) {
            Flash::error('Rol no encontrado');

            return redirect(route('roles.index'));
        }

        $this->rolesRepository->delete($id);

        Flash::success('Rol eliminado.');

        return redirect(route('roles.index'));
    }
}
