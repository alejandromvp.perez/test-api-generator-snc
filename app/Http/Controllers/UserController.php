<?php

namespace App\Http\Controllers;

use Flash;
use App\Role;
use App\User;
use Response;
use App\Control;
use App\Profile;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * listar todos los usuarios con roles
     */
    public function indexRoles(Request $request)
    {
        //lista todos los datos de los usuarios
        $users=User::name($request['name'])->orderBy('id','asc')->paginate(5);
        $roles=Role::all();
        return view('users.indexRoles',['users'=>$users, 'roles'=>$roles]);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->validator($request->all())->validate();

        /*opciones de registro y activacion*/
        $registro= Control::find(1);

        switch ($registro->value) {
            case 1:
                # registro con logeo automatico
                $this->guard()->login( $user=$this->create($request->all()) ); 

                //creacion del perfil
                $profile= Profile::create(['user_id'=>$user->id]);

                //asignacion del rol
                $role_invitado = Role::where('name', 'invitado')->first();
                $user->attachRole($role_invitado);

                return redirect($this->redirectPath());
            break;

            case 2:
                # registro con envio de email
                $contraseña= $request['password'];

                //creacion del perfil
                $profile= Profile::create(['user_id'=>$user->id]);

                //asignacion del rol
                $role_invitado = Role::where('name', 'invitado')->first();
                $user->attachRole($role_invitado);

                 //envio de email
                Mail::to($user->email, $user->name)
                    ->send(new WelcomeEmail($user,$contraseña));
                
                flash('Usuario creado con exito, revise su email para obtener sus credenciales', 'success');
                return view('auth.login');
            break;

            case 3:
                # registro con redireccion a una vista
                $contraseña= $request['password'];

                $input = $request->all();
                $user=$this->userRepository->create($input);
                //creacion del perfil
                $profile= Profile::create(['user_id'=>$user->id]);

                //asignacion del rol
                $role_invitado = Role::where('name', 'invitado')->first();
                $user->attachRole($role_invitado);
                
                flash('Usuario creado con exito, se informa que el usuario y clave de acceso son los siguientes: <br> <br>
                    <strong>Nombre: '.$user->name.'  </strong><br> 
                    <strong>Email: '.$user->email.' </strong> <br> 
                    <strong>Clave: '.$contraseña.'</strong> <br> <br>
                    Por favor tome las precauciones necesarias para guardar su nueva clave.', 'info');

                return redirect(route('users.index'));
                
            break;
        }
    }

     /**
     * [postAssignRoles asiga roles al usuario]
     * @param  Request $request [email del usaurio, variable que representa el rol]
     * @return [type]           [vista]
     */
    public function postAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        $roles=Role::all();
        //dd($request);

        $user->roles()->detach();
        foreach ($roles as $role) {
            # asignar roles
            if ($request[$role->name]) {
                $user->attachRole(Role::where('name', $role->name)->first());
            }
        }

        flash('Roles asignados', 'success');
        return redirect()->back();
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        $user = $this->userRepository->update($request->all(), $id);

        Flash::success('Usuario actualizado.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('Usuario eliminado.');

        return redirect(route('users.index'));
    }
}
