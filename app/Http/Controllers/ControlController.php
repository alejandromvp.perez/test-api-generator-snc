<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Http\Requests;
use App\DataTables\ControlDataTable;
use App\Repositories\ControlRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateControlRequest;
use App\Http\Requests\UpdateControlRequest;

class ControlController extends AppBaseController
{
    /** @var  ControlRepository */
    private $controlRepository;

    public function __construct(ControlRepository $controlRepo)
    {
        $this->controlRepository = $controlRepo;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'value' => 'required',
            'description' => 'required',
        ]);
    }

    /**
     * Display a listing of the Control.
     *
     * @param ControlDataTable $controlDataTable
     * @return Response
     */
    public function index(ControlDataTable $controlDataTable)
    {
        return $controlDataTable->render('controls.index');
    }

    /**
     * Show the form for creating a new Control.
     *
     * @return Response
     */
    public function create()
    {
        return view('controls.create');
    }

    /**
     * Store a newly created Control in storage.
     *
     * @param CreateControlRequest $request
     *
     * @return Response
     */
    public function store(CreateControlRequest $request)
    {
        $this->validator($request->all())->validate();
        
        $input = $request->all();

        $control = $this->controlRepository->create($input);

        Flash::success('Control creado.');

        return redirect(route('controls.index'));
    }

    /**
     * Display the specified Control.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $control = $this->controlRepository->findWithoutFail($id);

        if (empty($control)) {
            Flash::error('Control no encontrado');

            return redirect(route('controls.index'));
        }

        return view('controls.show')->with('control', $control);
    }

    /**
     * Show the form for editing the specified Control.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $control = $this->controlRepository->findWithoutFail($id);

        if (empty($control)) {
            Flash::error('Control no encontrado');

            return redirect(route('controls.index'));
        }

        return view('controls.edit')->with('control', $control);
    }

    /**
     * Update the specified Control in storage.
     *
     * @param  int              $id
     * @param UpdateControlRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateControlRequest $request)
    {
        $control = $this->controlRepository->findWithoutFail($id);

        if (empty($control)) {
            Flash::error('Control no encontrado');

            return redirect(route('controls.index'));
        }

        $control = $this->controlRepository->update($request->all(), $id);

        Flash::success('Control actualizado.');

        return redirect(route('controls.index'));
    }

    /**
     * Remove the specified Control from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $control = $this->controlRepository->findWithoutFail($id);

        if (empty($control)) {
            Flash::error('Control no encontrado');

            return redirect(route('controls.index'));
        }

        $this->controlRepository->delete($id);

        Flash::success('Control eliminado.');

        return redirect(route('controls.index'));
    }
}
