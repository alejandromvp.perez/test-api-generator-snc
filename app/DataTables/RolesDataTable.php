<?php

namespace App\DataTables;

use App\Role;
use Form;
use Yajra\Datatables\Services\DataTable;

class RolesDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'roles.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $roles = Role::query();

        return $this->applyScopes($roles);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                     ['extend' => 'print', 'text' => 'Imprimir'],
                    ['extend' => 'reset', 'text' => 'Reiniciar'],
                    ['extend' => 'reload', 'text' => 'Recargar'],
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Exportar',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf'
                         ],
                    ],
                    ['extend' => 'colvis', 'text' => 'Columnas visibles']
                ],
                'language' => ['url' => '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json']
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'display_name' => ['name' => 'display_name', 'data' => 'display_name'],
            'description' => ['name' => 'description', 'data' => 'description'],
            'deleted_at'=>[ 'name'=> 'deleted_at', 'width'=> '30px', 'class'=> 'text-center', 'searchable'=> 'true' ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'roles';
    }
}
