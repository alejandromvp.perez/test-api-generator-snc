@extends('layouts.app')

@section('content')
    <section class="content-header">
     
    </section>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-primary">
                {!! Form::open(['route' =>['roles.assignPermissions',$role->id] ,'method'=>'POST']) !!}

                <div class="box-header with-border">
                    <h3 class="box-title">Editar Permisos del Rol:  <b>{{$role->name}}</b></h3>
                    <div class="pull-right">
                                    {!! Form::submit('Asignar', ['class'=>'btn btn-sm btn-primary']) !!}
                                </div>

                </div>
                    
                <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                
                                <table class="table table-bordered">
                                    <br>
                                    <thead>
                                        <tr>
                                            <th>Permisos</th>
                                            <th> Descripcion</th>
                                            <th> Operaciones</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            @foreach($permissions as $permission)
                                                <tr>
                                                    <td> {{ $permission->name }} </td>
                                                    <td> {{ $permission->description }} </td>
                                                    <td> <input type="checkbox" class="js-switch" name={{$permission->name}} {{ $user->can( $permission->name) ? 'checked' :''}}></td>
                                                    {{ csrf_field() }}
                                                </tr>
                                            @endforeach   
                                        </tbody> 
                                     
                                </table>
                                
                                <div class="text-center">
                                     {!! $permissions->links() !!} 
                                </div>
                            </div>
                        </div>
                	
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
  <script>
    //switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    var html = { size: 'small', color: '#3c8dbc' };
    elems.forEach(function(html) {
      var switchery = new Switchery(html, { size: 'small', color: '#3c8dbc' });
    });
  </script>
@endsection