@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="content-header">
			<h1>
				API Generator
				<small>Version 1.0</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
			</ol>
		</div>
	</div>

	<div class="row">
		<!-- Main content -->
		<section class="content">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<!-- ./col -->
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3><i class="fa fa-code"></i></h3>
							<p>Swagger - Interfaz grafica</p>
						</div>
						<div class="icon">
							<i class="fa fa-code"></i>
						</div>
						<a href="{!! url('api/docs') !!}" class="small-box-footer">Hacer click <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					@if(Entrust::can('asignarRoles-usuarios'))
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3><i class="ion ion-person-add"></i></h3>
							<p>Crear usuario</p>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="{!! route('users.create') !!}" class="small-box-footer">Hacer click <i class="fa fa-arrow-circle-right"></i></a>
					</div>
					@endif
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					@if(Entrust::can('listar-usuarios'))
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3><i class="fa fa-users"></i></h3>
							<p>Usuarios </p>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="{!! route('users.index') !!}" class="small-box-footer">Hacer click <i class="fa fa-arrow-circle-right"></i></a>
					</div>
					@endif

				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					@if(Entrust::can('listar-roles'))
					<div class="small-box bg-red">
						<div class="inner">
							<h3><i class="fa fa-user-secret"></i></h3>
							<p>Roles</p>
						</div>
						<div class="icon">
							<i class="fa fa-user-secret"></i>
						</div>
						<a href="{!! route('roles.index') !!}" class="small-box-footer">Hacer click <i class="fa fa-arrow-circle-right"></i></a>
					</div>
					@endif
				</div>
				<!-- ./col -->
			</div>
			<!-- /.row -->
		</section>
	</div>

	<!-- Main row -->
      <div class="row" style="height: 345px;">
        <!-- Left col -->
        <div class="col-md-6">
        	<!-- Informacion -->
        	<div class="box box-primary">
        		<div class="box-header with-border">
        			<h3 class="box-title">Informacion</h3>

        			<div class="box-tools pull-right">
        				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        				</button>
        				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        			</div>
        		</div>
        		<!-- /.box-header -->
        		<div class="box-body no-padding">
        			<div class="row">
        				<div class="col-md-12 col-sm-12">
        					<div class="pad">
        						<div class="nav-tabs-custom">
        							<ul class="nav nav-tabs pull-right">
        								<li class="active"><a href="#tab_1-1" data-toggle="tab">Informacion 1</a></li>
        								<li><a href="#tab_2-2" data-toggle="tab">Informacion 2</a></li>
        								<li><a href="#tab_3-2" data-toggle="tab">Informacion 3</a></li>
        								<li class="pull-left header"><i class="fa fa-th"></i> </li>
        							</ul>
        							<div class="tab-content">
        								<div class="tab-pane active" id="tab_1-1">
        									<b>How to use:</b>

        									<p>Exactly like the original bootstrap tabs except you should use
        										the custom wrapper <code>.nav-tabs-custom</code> to achieve this style.</p>
        										A wonderful serenity has taken possession of my entire soul,
        										like these sweet mornings of spring which I enjoy with my whole heart.
        										I am alone, and feel the charm of existence in this spot,
        										which was created for the bliss of souls like mine. I am so happy,
        										my dear friend, so absorbed in the exquisite sense of mere tranquil existence,
        										that I neglect my talents. I should be incapable of drawing a single stroke
        										at the present moment; and yet I feel that I never was a greater artist than now.
        									</div>
        									<!-- /.tab-pane -->
        									<div class="tab-pane" id="tab_2-2">
        										The European languages are members of the same family. Their separate existence is a myth.
        										For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
        										in their grammar, their pronunciation and their most common words. Everyone realizes why a
        										new common language would be desirable: one could refuse to pay expensive translators. To
        										achieve this, it would be necessary to have uniform grammar, pronunciation and more common
        										words. If several languages coalesce, the grammar of the resulting language is more simple
        										and regular than that of the individual languages.
        									</div>
        									<!-- /.tab-pane -->
        									<div class="tab-pane" id="tab_3-2">
        										Lorem Ipsum is simply dummy text of the printing and typesetting industry.
        										Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
        										when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        										It has survived not only five centuries, but also the leap into electronic typesetting,
        										remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
        										sheets containing Lorem Ipsum passages, and more recently with desktop publishing software
        										like Aldus PageMaker including versions of Lorem Ipsum.
        									</div>
        									<!-- /.tab-pane -->
        								</div>
        								<!-- /.tab-content -->
        							</div>
        							<div id="world-map-markers"></div>
        						</div>
        					</div>
        				<!-- /.col -->
        			</div>
        			<!-- /.row -->
        		</div>
        		<!-- /.box-body -->
        	</div>
        </div>
        <!-- /.col -->
		<!-- Right col -->
        <div class="col-md-6">
        	<!-- Conceptos sistemas -->
        	<div class="box box-danger">
        		<div class="box-header with-border">
        			<h3 class="box-title">Conceptos sobre el sistemas</h3>

        			<div class="box-tools pull-right">
        				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        				</button>
        				<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        			</div>
        		</div>
        		<!-- /.box-header -->
        		<div class="box-body no-padding">
        			<div class="row">
        				<div class="col-md-12 col-sm-12">
        					<div class="pad">
        						<div class="box box-solid">
        							<!-- /.box-header -->
        							<div class="box-body">
        								<div class="box-group" id="accordion">
        									<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        									<div class="panel box box-primary">
        										<div class="box-header with-border">
        											<h4 class="box-title">
        												<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        													Que es una APIRest
        												</a>
        											</h4>
        										</div>
        										<div id="collapseOne" class="panel-collapse collapse in">
        											<div class="box-body">
        												Una API REST es una una biblioteca apoyada totalmente en el estándar HTTP. Visto de una forma más sencilla, una API REST es un servicio que nos provee de funciones que nos dan la capacidad de hacer uso de un servicio web que no es nuestro, dentro de una aplicación propia, de manera segura.
        											</div>
        										</div>
        									</div>
        									<div class="panel box box-success">
        										<div class="box-header with-border">
        											<h4 class="box-title">
        												<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        													Que es Swagger
        												</a>
        											</h4>
        										</div>
        										<div id="collapseTwo" class="panel-collapse collapse">
        											<div class="box-body">
        												Cuando hablamos de Swagger nos referimos a una serie de reglas, especificaciones y herramientas que nos ayudan a documentar nuestras APIs. <br><br> De esta manera, podemos realizar documentación que sea realmente útil para las personas que la necesitan. Swagger nos ayuda a crear documentación que todo el mundo entienda.
        											</div>
        										</div>
        									</div>
        									
        								</div>
        							</div>
        							<!-- /.box-body -->
        						</div>
        					</div>
        				<!-- /.col -->
        			</div>
        			<!-- /.row -->
        		</div>
        		<!-- /.box-body -->
        	</div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
</div>
@endsection
