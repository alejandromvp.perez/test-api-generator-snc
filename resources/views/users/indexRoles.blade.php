@extends('layouts.app')

@section('content')

  <section class="content-header">


  </section>
  <div class="content">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-6">
             <h4>Asignar roles</h4>
          </div>
          <div class="col-md-6 pull-right" >
            <div class="pull-right">
              {!! Form::open(['route' => 'users.indexRoles','method'=>'GET', 'class'=>'navbar-form navbar-left', 'role'=>'search']) !!}
                <div class="form-group">
                {!! Form::text('name', null,['class'=>'form-control', 'placeholder'=>'usuario'] ) !!} 
                </div>
                {!! Form::submit('Buscar', ['class'=>'btn btn-primary']) !!}
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class=" col-md-12">
            @include('flash::message')
          </div>
        </div>
        <br>
        <table class="table">
          <thead>
            <tr>
              <th>usuario</th>
              <th>email</th>
              @foreach($roles as $role)
              <td> {{ $role->name}}</td>
              @endforeach
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              {!! Form::open(['route' => 'users.assignRoles','method'=>'POST']) !!}
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td><input type="hidden" name="email" value="{{$user->email}}">
              @foreach($roles as $role)
              <td> <input type="checkbox" class="js-switch"  name={{$role->name}} {{ $user->hasRole( $role->name) ? 'checked' :''}}></td>
              @endforeach

              <td>  {!! Form::submit('Asignacion de Roles', ['class'=>'btn btn-sm btn-primary']) !!}  </td> 
              {{ csrf_field() }}
              {!! Form::close() !!}
            </tr>
            @endforeach                       
          </tbody>
        </table>
        <div class="text-center">
         {!! $users->links() !!} 
        </div>
     </div>
   </div>
  </div>
@endsection

@section('scripts')
  <script>
    //switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    var html = { size: 'small', color: '#3c8dbc' };
    elems.forEach(function(html) {
      var switchery = new Switchery(html, { size: 'small', color: '#3c8dbc' });
    });
  </script>
@endsection