@extends('layouts.app')

@section('content')
    <section class="content-header">
 
    </section>
    <div class="content">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Crear Usuario</h3>
                </div>
                <div class="box-body">
                    @include('adminlte-templates::common.errors')

                    <div class="register-box-body ">
                        {!! Form::open(['route' => 'users.store']) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}"/>
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Clave" name="password"/>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="password" class="form-control" placeholder="Confirmar clave" name="password_confirmation"/>
                                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                            
                            <div class="col-md-12">
                                <div class="col-xs-5 ">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Registrarse </button>
                                </div><!-- /.col -->
                                <div class="col-xs-1">
                                    
                                </div><!-- /.col -->
                                <div class="col-xs-5">

                                </div><!-- /.col -->
                                
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
