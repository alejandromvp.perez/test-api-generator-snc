<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="col-sm-12">
    {!! Form::label('password', 'Password:') !!}
</div>
<div class="col-sm-6">
    {!! Form::password('password', ['class' => 'form-control', 'id'=>'passwordInput']) !!}
</div>
<div class="col-sm-6">
    <a class="btn btn-default" id="ramdonValue">Generar clave</a>
</div>

<div class="form-group col-sm-6 ">
   
</div>
<!-- Active Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('active', 'Active:') !!}
    {!! Form::number('active', null, ['class' => 'form-control',]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>

</div>
 @include('layouts.partial.scripts')
<script type="text/javascript">
    var gRandLength = 7;    

    $(document).ready(function() {   
        $('#ramdonValue').click(function() {   
            var num = Math.floor(1 + (Math.random() * Math.pow(10, gRandLength)));
            $('#passwordInput').val(num);
        });
    });

</script>

