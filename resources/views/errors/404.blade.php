@extends('layouts.app')
@section('content')
    <section class="content-header">
      <h1>
        404 No encontrado
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! No encontrado.</h3>

          <p>	
			No pudimos encontrar la página que estabas buscando. Mientras tanto, puede  <a href="../../index.html">regresar al inicio</a>.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>

@endsection
