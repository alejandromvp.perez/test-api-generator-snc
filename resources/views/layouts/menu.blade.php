<li class="header">Paginas</li>
	<li><a href="{!! url('home') !!}"><i class="fa fa-circle-o"></i>Inicio</a></li>
</li>

<li class="header">DOCUMENTACION</li>
<li class="">
	<a href="#">
		<i class="fa fa-code"></i>
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Swagger
		</span>
	</a>
	<ul class="treeview-menu">
		<li><a href="{!! url('docs') !!}"><i class="fa fa-circle-o"></i>JSON</a></li>
		<li><a href="{!! url('api/docs') !!}"><i class="fa fa-circle-o"></i>Interfaz grafica</a></li>
	</ul>
</li>


@if(Entrust::hasRole(['administrador', 'operador']))
<li class="header">OPCIONES</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-users"></i>
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Usuarios
		</span>
	</a>
	<ul class="treeview-menu">
		@if(Entrust::can('listar-usuarios'))
		<li><a href="{!! route('users.index') !!}"><i class="fa fa-circle-o"></i>Listado</a></li>
		@endif
		@if(Entrust::can('crear-usuarios'))
		<li><a href="{!! route('users.create') !!}"><i class="fa fa-circle-o"></i>Crear</a></li>
		@endif
		@if(Entrust::can('crear-usuarios'))
		<li><a href="{!! route('users.indexRoles') !!}"><i class="fa fa-circle-o"></i>Asignar roles</a></li>
		@endif
	</ul>
</li>
@endif

@if(Entrust::hasRole(['administrador', 'operador']))
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-user-secret"></i> 
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Roles
		</span>
	</a>
	<ul class="treeview-menu">
		@if(Entrust::can('listar-roles'))
		<li><a href="{!! route('roles.index') !!}"><i class="fa fa-circle-o"></i>Listado</a></li>
		@endif
		@if(Entrust::can('crear-roles'))
		<li><a href="{!! route('roles.create') !!}"><i class="fa fa-circle-o"></i>Crear</a></li>
		@endif
	</ul>
</li>
@endif

@if(Entrust::hasRole('administrador'))
<li class="{{ Request::is('permissions*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-unlock-alt"></i> 
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Permisos
		</span>
	</a>
	<ul class="treeview-menu">
		<li><a href="{!! route('permissions.index') !!}"><i class="fa fa-circle-o"></i>Listado</a></li>
		<li><a href="{!! route('permissions.create') !!}"><i class="fa fa-circle-o"></i>Crear</a></li>
	</ul>
</li>
@endif

@if(Entrust::hasRole(['administrador', 'operador']))
<li class="{{ Request::is('controls*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-cog"></i> 
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Controles
		</span>
	</a>
	<ul class="treeview-menu">
		<li><a href="{!! route('controls.index') !!}"><i class="fa fa-circle-o"></i>Listado</a></li>
		<li><a href="{!! route('controls.create') !!}"><i class="fa fa-circle-o"></i>Crear</a></li>
	</ul>
</li>
@endif

@if(Entrust::hasRole(['administrador']))
<li class="{{ Request::is('logs*') ? 'active' : '' }}">
	<a href="#">
		<i class="fa fa-plus"></i>
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>Otros
		</span>
	</a>
	<ul class="treeview-menu">
		<li><a href="{!! url('/logs') !!}"><i class="fa fa-circle-o"></i>logs</a></li>
	</ul>
</li>
@endif
