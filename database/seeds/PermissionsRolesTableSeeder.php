<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;
class PermissionsRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //seleccion de roles
        $invitado = Role::where('name', 'invitado')->first();
        $operador = Role::where('name', 'operador')->first();
        $administrador = Role::where('name', 'administrador')->first();

        //seleccion de permisos de usuarios
        $listarUsuarios = Permission::where('name', 'listar-usuarios')->first();
        $crearUsuarios = Permission::where('name', 'crear-usuarios')->first();
        $editarUsuarios = Permission::where('name', 'editar-usuarios')->first();
        $eliminarUsuarios = Permission::where('name', 'eliminar-usuarios')->first();
        $asignarRolesUsuarios = Permission::where('name', 'asignarRoles-usuarios')->first();
        $editarPerfilUsuarios = Permission::where('name', 'editarPerfil-usuarios')->first();
        
        //seleccion de permisos de roles
        $listarRoles = Permission::where('name', 'listar-roles')->first();
        $crearRoles = Permission::where('name', 'crear-roles')->first();
        $editarRoles = Permission::where('name', 'editar-roles')->first();
        $eliminarRoles = Permission::where('name', 'eliminar-roles')->first();
        $asignarPermisosRoles = Permission::where('name', 'asignarPermisos-roles')->first();
        
        //seleccion de permisos de permisos
        $listarPermisos = Permission::where('name', 'listar-permisos')->first();
        $crearPermisos = Permission::where('name', 'crear-permisos')->first();
        $editarPermisos = Permission::where('name', 'editar-permisos')->first();
        $eliminarPermisos = Permission::where('name', 'eliminar-permisos')->first();

        //asigar permisos a los roles
        $invitado->attachPermission($editarPerfilUsuarios);
        
        $operador->attachPermissions(array($editarPerfilUsuarios,$listarUsuarios,$crearUsuarios));
        
        $administrador->attachPermissions(array($listarUsuarios,$crearUsuarios,$editarUsuarios,$eliminarUsuarios,$asignarRolesUsuarios,$editarPerfilUsuarios,$listarRoles,$crearRoles,$editarRoles,$eliminarRoles,$asignarPermisosRoles,$listarPermisos,$crearPermisos,$editarPermisos,$eliminarPermisos));
    }
}
