<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creacion de permisos basicos
        //Usuarios
        $listarUsuarios=new Permission(); 
        $listarUsuarios->name = 'listar-usuarios'; 
        $listarUsuarios->display_name = 'Mostar usuarios'; 
        $listarUsuarios->description = 'Muestra los usuarios del sistema'; 
        $listarUsuarios->save();

        $crearUsuarios=new Permission(); 
        $crearUsuarios->name = 'crear-usuarios'; 
        $crearUsuarios->display_name = 'crear usuarios'; 
        $crearUsuarios->description = 'Crea usuarios del sistema'; 
        $crearUsuarios->save();

        $editarUsuarios=new Permission(); 
        $editarUsuarios->name = 'editar-usuarios'; 
        $editarUsuarios->display_name = 'Editar usuarios'; 
        $editarUsuarios->description = 'Modifica usuarios del sistema'; 
        $editarUsuarios->save();

        $eliminarUsuarios=new Permission(); 
        $eliminarUsuarios->name = 'eliminar-usuarios'; 
        $eliminarUsuarios->display_name = 'eliminar usuarios'; 
        $eliminarUsuarios->description = 'Elimina usuarios del sistema'; 
        $eliminarUsuarios->save();

        $asignaRolesUsuarios=new Permission(); 
        $asignaRolesUsuarios->name = 'asignarRoles-usuarios'; 
        $asignaRolesUsuarios->display_name = 'Asignar roles'; 
        $asignaRolesUsuarios->description = 'Coloca roles a los usuarios del sistema'; 
        $asignaRolesUsuarios->save();

        $editarPerfilUsuarios=new Permission(); 
        $editarPerfilUsuarios->name = 'editarPerfil-usuarios'; 
        $editarPerfilUsuarios->display_name = 'Editar perfil usuarios'; 
        $editarPerfilUsuarios->description = 'Mofifica tus datos del sistema '; 
        $editarPerfilUsuarios->save();

        //Roles
        $listarRoles=new Permission(); 
        $listarRoles->name = 'listar-roles'; 
        $listarRoles->display_name = 'Mostar Roles '; 
        $listarRoles->description = 'Muestra los roles del sistema'; 
        $listarRoles->save();

        $crearRoles=new Permission(); 
        $crearRoles->name = 'crear-roles'; 
        $crearRoles->display_name = 'crear Roles '; 
        $crearRoles->description = 'Crea los roles del sistema'; 
        $crearRoles->save();

        $editarRoles=new Permission(); 
        $editarRoles->name = 'editar-roles'; 
        $editarRoles->display_name = 'Editar Roles '; 
        $editarRoles->description = 'Edita los roles del sistema'; 
        $editarRoles->save();

        $eliminarRoles=new Permission(); 
        $eliminarRoles->name = 'eliminar-roles'; 
        $eliminarRoles->display_name = 'Eliminar Roles '; 
        $eliminarRoles->description = 'Elimina los roles del sistema'; 
        $eliminarRoles->save();

        $asignarPermisosRoles=new Permission(); 
        $asignarPermisosRoles->name = 'asignarPermisos-roles'; 
        $asignarPermisosRoles->display_name = 'Asignar permisos '; 
        $asignarPermisosRoles->description = 'Asigna los permisos a los roles'; 
        $asignarPermisosRoles->save();

        //Permisos
        $listarPermisos=new Permission(); 
        $listarPermisos->name = 'listar-permisos'; 
        $listarPermisos->display_name = 'Mostar permisos '; 
        $listarPermisos->description = 'Muestra los permisos del sistema'; 
        $listarPermisos->save();

        $crearPermisos=new Permission(); 
        $crearPermisos->name = 'crear-permisos'; 
        $crearPermisos->display_name = 'Crear permisos '; 
        $crearPermisos->description = 'crea los permisos del sistema'; 
        $crearPermisos->save();

        $editarPermisos=new Permission(); 
        $editarPermisos->name = 'editar-permisos'; 
        $editarPermisos->display_name = 'Editar permisos '; 
        $editarPermisos->description = 'Edita los permisos del sistema'; 
        $editarPermisos->save();

        $eliminarPermisos=new Permission(); 
        $eliminarPermisos->name = 'eliminar-permisos'; 
        $eliminarPermisos->display_name = 'Eliminar permisos '; 
        $eliminarPermisos->description = 'Elimina los permisos del sistema'; 
        $eliminarPermisos->save();
    }
}
