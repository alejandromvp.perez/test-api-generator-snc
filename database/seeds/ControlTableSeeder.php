<?php

use Illuminate\Database\Seeder;
use App\Control;

class ControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $control = new Control();
        $control->name = "Registro y activacion de usuarios";
        $control->value = 3;
        $control->description="Opciones para la acticavion de usuarios. 1:registro con logeo automatico  2:registro con envio de email 3:registro con redireccion a una vista ";
        $control->save();
    }
}
