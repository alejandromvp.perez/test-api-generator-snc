<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //Creacuon de roles basicos
        $administrador=new Role(); 
        $administrador->name = 'administrador'; 
        $administrador->display_name = 'administrador del sistema'; 
        $administrador->description = 'Usuario tipo admin del sistema';
        $administrador->save();


        $operador = new Role(); 
        $operador->name = 'operador'; 
        $operador->display_name = 'usuario operador'; 
        $operador->description ='Usuario tipo operador del sistema'; 
        $operador->save();

        $invitado = new Role(); 
        $invitado->name = 'invitado'; 
        $invitado->display_name = 'usuario invitado'; 
        $invitado->description ='Usuario tipo invitado del sistema'; 
        $invitado->save();
    }
}
