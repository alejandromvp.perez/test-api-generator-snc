<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creacion de usuarios con roles asignados
        $role_invitado = Role::where('name', 'invitado')->first();
        $role_administrador = Role::where('name', 'administrador')->first();
        $role_operador = Role::where('name', 'operador')->first();

        $admin = new User();
        $admin->name='admin';
        $admin->email='admin@ejemplo.com';
        $admin->password= 'admin';
        $admin->save(); 
        $admin->attachRole($role_administrador);

        $operador = new User();
        $operador->name='operador';
        $operador->email='operador@ejemplo.com';
        $operador->password= 'operador';
        $operador->save();
        $operador->attachRole($role_operador);
       
      	$invitado = new User();
        $invitado->name='invitado';
        $invitado->email='invitado@ejemplo.com';
        $invitado->password= 'invitado';
        $invitado->save();
        $invitado->attachRole($role_invitado);
    }
}
