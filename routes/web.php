<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


 /*
|--------------------------------------------------------------------------
| Rutas visibles para todos
|--------------------------------------------------------------------------
|
|
*/
Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Rutas visibles para permisos nuevos
|--------------------------------------------------------------------------
|
|
*/
Route::group(['middleware' => ['permission:asignarRoles-usuarios']], function () {
	Route::get('users/indexRoles', [
		'uses'=>'UserController@indexRoles',
		'as'=>'users.indexRoles',
	]);

	Route::post('users/assignRoles', [
		'uses'=>'UserController@postAssignRoles',
		'as'=>'users.assignRoles',
	]);	
});


//roles
Route::get('roles/{role}/editPermissions', [
	'uses'=>'RolesController@editPermissions',
	'as'=>'roles.editPermissions',
	'middleware' => ['permission:asignarPermisos-roles']
]);

Route::post('roles/{role}/assignPermissions', [
	'uses'=>'RolesController@postAssignPermissions',
	'as'=>'roles.assignPermissions',
	'middleware' => ['permission:asignarPermisos-roles']
]);




/*
|--------------------------------------------------------------------------
| Rutas visibles solo con rol invitado
|--------------------------------------------------------------------------
|
|
*/
Route::group(['middleware' => ['role:invitado','auth']], function () {
	

});

/*
|--------------------------------------------------------------------------
| Rutas visibles solo con rol operador
|--------------------------------------------------------------------------
|
|
*/
Route::group(['middleware' => ['role:operador','auth']], function () {
	

});


/*
|--------------------------------------------------------------------------
| Rutas visibles solo con rol administrador
|--------------------------------------------------------------------------
|
|
*/
Route::group(['middleware' => ['role:administrador','auth']], function () {
	

	Route::resource('permissions', 'PermissionController');

	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


/*
|--------------------------------------------------------------------------
| Rutas visibles para varios roles
|--------------------------------------------------------------------------
|
|
*/
Route::group(['middleware' => ['role:administrador|operador']], function () {

	Route::resource('roles', 'RolesController');

	Route::resource('users', 'UserController');

	Route::resource('controls', 'ControlController');
});



/*
|--------------------------------------------------------------------------
| Rutas visibles para permisos sobreescritos
|--------------------------------------------------------------------------
|
|
*/

//users
Route::group(['middleware' => ['permission:crear-usuarios']], function () {
	
	Route::get('users/create', [
		'uses'=>'UserController@create',
		'as'=>'users.create',
	]);

	Route::post('users', [
		'uses'=>'UserController@store',
		'as'=>'users.store',
	]);
});

Route::get('users', [
	'uses'=>'UserController@index',
	'as'=>'users.index',
	'middleware' => ['permission:listar-usuarios']
]);

Route::get('users/{user}', [
	'uses'=>'UserController@show',
	'as'=>'users.show',
	'middleware' => ['permission:listar-usuarios']
]);

Route::get('users/{user}/edit', [
	'uses'=>'UserController@edit',
	'as'=>'users.edit',
	'middleware' => ['permission:editar-usuarios']
]);

Route::put('users/{user}', [
	'uses'=>'UserController@update',
	'as'=>'users.update',
	'middleware' => ['permission:editar-usuarios']
]);

Route::delete('users/{user}', [
	'uses'=>'UserController@destroy',
	'as'=>'users.destroy',
	'middleware' => ['permission:eliminar-usuarios']
]);

//roles
Route::group(['middleware' => ['permission:crear-roles']], function () {
	
	Route::get('roles/create', [
		'uses'=>'RolesController@create',
		'as'=>'roles.create',
	]);

	Route::post('roles', [
		'uses'=>'RolesController@store',
		'as'=>'roles.store',
	]);
});

Route::get('roles', [
	'uses'=>'RolesController@index',
	'as'=>'roles.index',
	'middleware' => ['permission:listar-roles']
]);

Route::get('roles/{role}', [
	'uses'=>'RolesController@show',
	'as'=>'roles.show',
	'middleware' => ['permission:listar-roles']
]);

Route::get('roles/{role}/edit', [
	'uses'=>'RolesController@edit',
	'as'=>'roles.edit',
	'middleware' => ['permission:editar-roles']
]);

Route::put('roles/{role}', [
	'uses'=>'RolesController@update',
	'as'=>'roles.update',
	'middleware' => ['permission:editar-roles']
]);

Route::delete('roles/{user}', [
	'uses'=>'RolesController@destroy',
	'as'=>'roles.destroy',
	'middleware' => ['permission:eliminar-roles']
]);


Route::group(['middleware' => ['auth']], function () {
	
	Route::any(Config::get('swaggervel.doc-route').'/{page?}', function($page='api-docs.json') {
	    $filePath = Config::get('swaggervel.doc-dir') . "/{$page}";

	    if (File::extension($filePath) === "") {
	        $filePath .= ".json";
	    }
	    if (!File::Exists($filePath)) {
	        App::abort(404, "Cannot find {$filePath}");
	    }

	    $content = File::get($filePath);
	    return Response::make($content, 200, array(
	        'Content-Type' => 'application/json'
	    ));
	});

	Route::get(Config::get('swaggervel.api-docs-route'), function() {
	    if (Config::get('swaggervel.generateAlways')) {
	        $appDir = base_path()."/".Config::get('swaggervel.app-dir');
	        $docDir = Config::get('swaggervel.doc-dir');

	        if (!File::exists($docDir) || is_writable($docDir)) {
	            // delete all existing documentation
	            if (File::exists($docDir)) {
	                File::deleteDirectory($docDir);
	            }

	            File::makeDirectory($docDir);

	            $defaultBasePath = Config::get('swaggervel.default-base-path');
	            $defaultApiVersion = Config::get('swaggervel.default-api-version');
	            $defaultSwaggerVersion = Config::get('swaggervel.default-swagger-version');
	            $excludeDirs = Config::get('swaggervel.excludes');

	            $swagger =  \Swagger\scan($appDir, [
	                'exclude' => $excludeDirs
	                ]);

	            $filename = $docDir . '/api-docs.json';
	            file_put_contents($filename, $swagger);
	        }
	    }

	    if (Config::get('swaggervel.behind-reverse-proxy')) {
	        $proxy = Request::server('REMOTE_ADDR');
	        Request::setTrustedProxies(array($proxy));
	    }

	    Blade::setEscapedContentTags('{{{', '}}}');
	    Blade::setContentTags('{{', '}}');

	    //need the / at the end to avoid CORS errors on Homestead systems.
	    $response = response()->view('swaggervel::index', array(
	        'secure'         => Request::secure(),
	        'urlToDocs'      => url(Config::get('swaggervel.doc-route')),
	        'requestHeaders' => Config::get('swaggervel.requestHeaders'),
	        'clientId'       => Request::input("client_id"),
	        'clientSecret'   => Request::input("client_secret"),
	        'realm'          => Request::input("realm"),
	        'appName'        => Request::input("appName"),
	        )
	    );

	    //need the / at the end to avoid CORS errors on Homestead systems.
	    /*$response = Response::make(
	        View::make('swaggervel::index', array(
	                'secure'         => Request::secure(),
	                'urlToDocs'      => url(Config::get('swaggervel.doc-route')),
	                'requestHeaders' => Config::get('swaggervel.requestHeaders') )
	        ),
	        200
	    );*/

	    if (Config::has('swaggervel.viewHeaders')) {
	        foreach (Config::get('swaggervel.viewHeaders') as $key => $value) {
	            $response->header($key, $value);
	        }
	    }

	    return $response;
	});
});